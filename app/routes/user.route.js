module.exports = app => {
    const userdata = require("../controllers/userdata.controller.js");
    const userbio = require("../controllers/userbio.controller.js");
  
    var router = require("express").Router();
  
    // Create a new User
    router.post("/", userdata.create);
  
    // Retrieve all Users
    router.get("/", userdata.findAll);
  
    // Retrieve a User with username
    router.get("/:username", userdata.findOne);

     // Upadate a User with username -> to update password
     router.put("/:username", userdata.update);

    // Create a new Userbiodata
    router.post("/bio", userbio.create);

    // Retrieve a Userbio by UserId
    router.get("/bio/:userId", userbio.findOne);

    router.delete("/bio/:userId", userbio.delete);

     // Retrieve all Userbio
     router.get("/userbio/bio", userbio.findAll);

    app.use('/api/user', router);
  };