const dbConfig = require('../config/db.config.js');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.userdata = require('./userdata.model.js')(sequelize, Sequelize);
db.userbio = require('./userbio.model.js')(sequelize, Sequelize);
db.usergame = require('./usergame.model.js')(sequelize, Sequelize);

db.userdata.hasMany(db.usergame, { 
  foreignKey : 'userId',
  as: 'history',
  onDelete: 'CASCADE'
});
// db.usergame.belongsTo(db.userdata, {
//   foreignKey: "userId",
//   as: "user",
// });

db.userdata.hasOne(db.userbio, { 
  foreignKey : 'userId',
  as: "biodata",
  onDelete: 'CASCADE'
});
// db.userbio.belongsTo(db.userdata, {
//   foreignKey: 'userId',
//     as: "user"
// });

module.exports = db;