module.exports = (sequelize, Sequelize) => {
    const UserData = sequelize.define("userdata", {
      username: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      password: {
        type: Sequelize.STRING(64),
        allowNull: false,
        is: /^[0-9a-f]{64}$/i
      }
    });
  
    return UserData;
};