module.exports = (sequelize, Sequelize) => {
    const UserBio = sequelize.define("userbio", {
      firstName: {
        type: Sequelize.STRING,
        allowNull: false
      },
      lastName: {
        type: Sequelize.STRING
      },
      birthDate: {
        type: Sequelize.DATE
      },
      level: {
          type: Sequelize.STRING
      }
    });
  
    return UserBio;
};