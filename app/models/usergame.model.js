module.exports = (sequelize, Sequelize) => {
    const UserGame = sequelize.define("usergame", {
      Score: {
        type: Sequelize.INTEGER
      },
      PlayTime: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      }
    });
  
    return UserGame;
};