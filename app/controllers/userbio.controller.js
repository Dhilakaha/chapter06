const db = require("../models");
const UserBio = db.userbio;
const Op = db.Sequelize.Op;

// Create and Save a new UserBio
exports.create = (req, res) => {
    // Validate request
  if (!req.body.firstName) {
    res.status(400).send({
      message: "Name can not be empty!"
    });
    return;
  }

  // Create a user
  const userbio = {
    firstName : req.body.firstName,
    lastName: req.body.lastName,
    birthDate: req.body.birthDate,
    level: req.body.level,
    userId: req.body.userId
  };

  // Save user in the database
  UserBio.create(userbio)
    .then(data => {
      //res.send(data);
      res.render('userprofile-view.ejs', { data: data })
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the userbio."
      });
    });
};

// Find a single user with an userId
exports.findOne = (req, res) => {
    const userId = req.params.userId;

    UserBio.findOne({ where: { userId: req.params.userId } })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving username with userId= " + userId
        });
      });
};

exports.findAll = (req, res) => {  
    const firstname = req.query.firstName;
    var condition = firstname ? { firstName: { [Op.iLike]: `%${firstname}%` } } : null;

    UserBio.findAll()
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving tutorials."
        });
      });
};

// Delete a userbio with the specified userid in the request
exports.delete = (req, res) => {
    const id = req.params.userId;
    console.log('DELETE');

    UserBio.destroy({
      where: { userId: id }
    })
      .then(nums => {
        res.send({
          message: "userbio with userId"+ id +" was deleted successfully!"
        });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all userbiodata with userId=" + id
        });
      });
};