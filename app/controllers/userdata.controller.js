const db = require("../models");
const UserData = db.userdata;
const Op = db.Sequelize.Op;

// Create and Save a new User
exports.create = (req, res) => {
    // Validate request
  if (!req.body.username) {
    res.status(400).send({
      message: "Name can not be empty!"
    });
    return;
  }

  // Create a Tutorial
  const userdata = {
    username : req.body.username,
    password: req.body.password,
  };

  // Save Tutorial in the database
  UserData.create(userdata)
    .then(data => {
      res.render('userprofile.ejs', { data: data })
      //res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the userdata."
      });
    });
};

// Retrieve all user from the database.
exports.findAll = (req, res) => {
    const username = req.query.username;
    var condition = username ? { username: { [Op.iLike]: `%${username}%` } } : null;
  
    UserData.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving tutorials."
        });
      });
};

// Find a single user with an username
exports.findOne = (req, res) => {
    const username = req.params.username;

    UserData.findOne({ where: { username: req.params.username } })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving username with username=" + username
        });
      });
};

// Update a userdata by the id in the request
exports.update = (req, res) => {
    const username = req.params.username;

    UserData.update(req.body, {
      where: { username: username }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "User "+ username +" was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update User with username=${username}. Maybe User was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating User with username=" + username
        });
      });
};