const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
let data = require('./src/data.json');

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.static('public'));

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
db.sequelize.sync();

// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
// });

app.get('/api/v1/images', (req, res) => {
  res.status(200).json(data);
});

app.get('/', (req, res) => {
  res.sendFile('./src/index.html', { root: __dirname });
})

app.get('/game', (req, res) => {
  res.sendFile('./src/game.html', { root: __dirname });
})

app.get('/register', (req, res) => {
  res.sendFile('./src/register.html', { root: __dirname });
})

app.get('/login', (req, res) => {
  res.sendFile('./src/login.html', { root: __dirname });
})

app.get('/userprofile', (req, res) => {
  res.render('userprofile.ejs', {})
})

// simple route
// app.get("/", (req, res) => {
//   res.json({ message: "Welcome to Traditional Game." });
// });

require("./app/routes/user.route")(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

// error handler
// define as the last app.use callback
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});